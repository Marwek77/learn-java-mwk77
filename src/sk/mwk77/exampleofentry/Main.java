package sk.mwk77.exampleofentry;

public class Main extends Entry {

    public static void main(String[] args) {

        String meno = readString("Ahoj ako so voláš?");
        System.out.println("Ahoj " + meno + ", veľmi ma teší!");

        double a = readNumber("Zadaj prvé číslo");
        double b = readNumber("Zadaj druhé číslo");
        double c = readNumber("Zadaj tretie číslo");

        double d = a * b * c;
        System.out.println("\nSúčin čísel " + a + " * " + b + " * " + c + " = " + d);

    }
}