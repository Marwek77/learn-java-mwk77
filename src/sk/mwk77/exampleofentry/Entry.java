package sk.mwk77.exampleofentry;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Entry {

        public static String readString(String call)
        {

            String string = "";

            InputStreamReader converter = new InputStreamReader(System.in);
            BufferedReader in = new BufferedReader(converter);

            try
                {
                    System.out.print(call + " (zadaj prosím reťazec): ");
                    string = in.readLine();
                }
            catch (Exception e)
                {
                }
            return string;
        }

        public static long readNumber(String call)
        {

            String string = "";
            long number = 0;

            InputStreamReader converter = new InputStreamReader(System.in);
            BufferedReader in = new BufferedReader(converter);

            boolean thereIsException;

            do {
                thereIsException = false;

            try
                {
                    System.out.print(call + " (celé číslo): ");
                    string = in.readLine();
                    number = Long.parseLong(string);
                }
            catch (Exception e)
                {
                    System.out.println("Zadaný reťazec nie je celé číslo.");
                    System.out.println("Opakujte zadanie!");
                    thereIsException = true;
                }

                } while (thereIsException);

            return number;
        }

}
