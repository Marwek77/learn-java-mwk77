package sk.mwk77.learnjava;

import sk.mwk77.learnjava.GrafickyObjekt.Stvorec;
import sk.mwk77.learnjava.GrafickyObjekt.Usecka;

public class VolanieGrafickyObjekt {

    public static void main(String[] args) {

        Stvorec stvorec = new Stvorec();
        Usecka usecka = new Usecka();

        stvorec.vykresliSa();
        usecka.vykresliSa();
    }
}
