package sk.mwk77.learnjava;

import java.util.Arrays;
import java.util.Random;

public class Example17 {
    public static void main(String[] args) {
        int [][] poleInt = new int[5][];

        for (int i = 0; i < poleInt.length; i++) {
                poleInt [i] = new int[(int) ((Math.random()*9)+1)];
                for (int j = 0; j < poleInt[i].length; j++) {
                    poleInt [i][j] = (int) (Math.random()*10);
                System.out.print(poleInt[i][j] + ", ");
            }
            System.out.println();
        }
        System.out.println(Arrays.deepToString(poleInt));
    }
}
