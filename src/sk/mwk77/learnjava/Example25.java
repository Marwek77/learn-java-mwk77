package sk.mwk77.learnjava;

public class Example25 {

    public static void main(String[] args) {
        String retazec = "qwerty";
        System.out.println(retazec);
        retazec = retazec + " alfa";
        System.out.println(retazec);

        StringBuffer ret = new StringBuffer("abcd");
        ret.append(" ide to");

        System.out.println(ret);
    }

}
