package sk.mwk77.learnjava.GrafickyObjekt;

import sk.mwk77.learnjava.interfaces.IGrafickyObjekt;

public class Stvorec implements IGrafickyObjekt {

    @Override
    public void vykresliSa() {
        System.out.println("Stvorec sa vykresluje");
    }
}
