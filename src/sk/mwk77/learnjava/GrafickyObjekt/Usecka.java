package sk.mwk77.learnjava.GrafickyObjekt;

import sk.mwk77.learnjava.interfaces.IGrafickyObjekt;

public class Usecka implements IGrafickyObjekt {

    @Override
    public void vykresliSa() {
        System.out.println("Usecka sa kresli");
    }
}
