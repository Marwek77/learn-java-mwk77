package sk.mwk77.learnjava;

import sun.security.util.Length;

public class Example26 {
    public static void main(String[] args) {
        double pi = Math.PI;
        String s = String.valueOf(pi);

        System.out.println(pi);
        System.out.println(String.valueOf(pi).length());
        System.out.println(s);
        System.out.println(s.length());
        System.out.println();

        pi += 1;
        s += 1;

        System.out.println(pi);
        System.out.println(String.valueOf(pi).length());
        System.out.println(s);
        System.out.println(s.length());

    }


}
