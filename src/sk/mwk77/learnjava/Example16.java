package sk.mwk77.learnjava;

import java.util.Arrays;
import java.util.Random;

public class Example16 {
    public static void main(String[] args) {
        int[] poleInt = new int[5];

        for (int i = 0; i < poleInt.length; i++) {
            poleInt[i] = (int) (Math.random()*10);
            System.out.println(poleInt[i]);
        }
        System.out.println(Arrays.toString(poleInt));

        Arrays.sort(poleInt);
        System.out.println(Arrays.toString(poleInt));
    }
}
