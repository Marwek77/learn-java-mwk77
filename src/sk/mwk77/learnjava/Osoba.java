package sk.mwk77.learnjava;

import java.util.Date;

public class Osoba {
    private Date datumNarodenia;

    public Osoba(Date datumNarodenia) {
        this.datumNarodenia = datumNarodenia;
    }

    public Date getDatumNarodenia() {
        return datumNarodenia;
    }

}
